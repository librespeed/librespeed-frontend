<?php
header('Content-Type: application/json; charset=utf-8');
header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0, s-maxage=0');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');
include_once '../_config.php';
$conn = new mysqli($MySql_hostname, $MySql_username, $MySql_password, $MySql_databasename);
$stmt = $conn->prepare("select id,name,server,dlURL,ulURL,pingURL,getIpURL,sponsorName,sponsorURL from submissions where status=10 order by name");
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($id,$name,$server,$dlURL,$ulURL,$pingURL,$getIpURL,$sponsorName,$sponsorURL);
$array=array();
while($stmt->fetch()){
    $a=array(
        "name" => $name,
        "server" => $server,
        "id" => $id,
        "dlURL" => $dlURL,
        "ulURL" => $ulURL,
        "pingURL" => $pingURL,
        "getIpURL" => $getIpURL,
        "sponsorName" => $sponsorName,
        "sponsorURL" => $sponsorURL
    );
    $array[]=$a;
}
echo json_encode($array);
?>
