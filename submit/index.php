<?php session_start();?>
<!DOCTYPE html>
<html>
<head>
<title>Submit a server - LibreSpeed</title>
<link rel="stylesheet" type="text/css" href="../style.css" />
<script type="text/javascript">
function I(id){return document.getElementById(id)}
String.prototype.isBlank=function(){return !this || /^\s*$/.test(this);}
if (!String.prototype.endsWith) {
  String.prototype.endsWith = function(searchString, position) {
      var subjectString = this.toString();
      if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
        position = subjectString.length;
      }
      position -= searchString.length;
      var lastIndex = subjectString.indexOf(searchString, position);
      return lastIndex !== -1 && lastIndex === position;
  };
}
function checkForm(){
    <?php if(isset($_SESSION["logged"])){ ?>return true; <?php } ?>
    if(I("name").value.isBlank()){alert("You must specify a name for your server");return false;}
    I("server").value=I("server").value.trim();
    if(!(/^(http:|https:)?\/\/[a-z|A-Z|0-9|-|.]*\/?$/.test(I("server").value))){alert("The server URL doesn't look right");return false;}
    if(!I("server").value.endsWith("/")) I("server").value=I("server").value+"/";
    if(I("server").value.isBlank()){alert("You must specify the URL to your server");return false;}
    if(I("dlURL").value.isBlank()){alert("Missing path to download test");return false;}
    if(I("dlURL").value[0]==="/") I("dlURL").value=I("dlURL").value.substring(1);
    if(I("ulURL").value.isBlank()){alert("Missing path to upload test");return false;}
    if(I("ulURL").value[0]==="/") I("ulURL").value=I("ulURL").value.substring(1);
    if(I("pingURL").value.isBlank()){alert("Missing path to ping/jitter test");return false;}
    if(I("pingURL").value[0]==="/") I("pingURL").value=I("pingURL").value.substring(1);
    if(I("getIpURL").value.isBlank()){alert("Missing path to getIP");return false;}
    if(I("getIpURL").value[0]==="/") I("getIpURL").value=I("getIpURL").value.substring(1);
    if(I("email").value.isBlank()){alert("We need your email to contact you back. It won't be made public, ever");return false;}
    if(!(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(I("email").value.trim()))) {alert("The email address is not valid");return false;}
    if(I("isSponsored").checked){
        if(I("sponsorName").value.isBlank()){alert("Missing sponsor name");return false;}
    }
    if(!I("check1").checked||!I("check2").checked){alert("You must tick the 2 checkboxes");return false;}
    if(I("captcha_code").value.isBlank()){alert("Please fill in the captcha");return false;}
    return true;
}
function enableDisableSponsorForm(){
    if(I("isSponsored").checked){
        I("sponsorName").disabled=undefined;
        I("sponsorURL").disabled=undefined;
        I("sponsorForm").style.opacity=1;
    }else{
        I("sponsorName").disabled="true";
        I("sponsorURL").disabled="true";
        I("sponsorForm").style.opacity=0.5;
    }
}
</script>
</head>
<body>
<h2>Submit a server</h2>
<p>If you have a LibreSpeed server and you want to have it added to this website, fill in the form below. We'll contact you back once we've checked that it works properly.</p>
<form action="submit.php" method="POST" onsubmit="return checkForm()">
<table>
<tr>
<td><label for="name">User friendly name</label></td><td><input type="text" id="name" name="name" placeholder="Location, Country (Provider)"/></td>
</tr>
<tr>
<td><label for="server">URL to the server<br/>(domain, not full path)</label></td><td><input type="text" id="server" name="server" placeholder="https://yourDomain.com/"/></td>
</tr>
<tr>
<td><label for="dlURL">Path to download test</label></td><td><input type="text" id="dlURL" name="dlURL" placeholder="garbage.php"/></td>
</tr>
<tr>
<td><label for="ulURL">Path to upload test</label></td><td><input type="text" id="ulURL" name="ulURL" placeholder="empty.php"/></td>
</tr>
<tr>
<td><label for="pingURL">Path to ping/jitter test</label></td><td><input type="text" id="pingURL" name="pingURL" placeholder="empty.php"/></td>
</tr>
<tr>
<td><label for="getIpURL">Path to getIP</label></td><td><input type="text" id="getIpURL" name="getIpURL" placeholder="getIP.php"/></td>
</tr>
<tr>
<td><label for="type">Type of backend</label></td><td><select name="type" id="type" value="php"><option value="php">PHP</option><option value="php_docker">PHP (Docker)</option><option value="go">Go</option><option value="node">Node</option><option value="custom">Custom</option></select></td>
</tr>
<tr>
<td><label for="email">Email address (will not be public)</label></td><td><input type="text" id="email" name="email"  placeholder="john.doe@email.com"/></td>
</tr>
<tr>
<td><label for="fullName">Your name (will not be public)</label></td><td><input type="text" id="fullName" name="fullName"  placeholder="John Doe"/></td>
</tr>
</table>
<input type="checkbox" id="isSponsored" name="isSponsored" onchange="enableDisableSponsorForm()"/><label for="isSponsored">Show a sponsor for this server</label>
<table id="sponsorForm">
<tr>
<td><label for="sponsorName">Sponsor name</label></td><td><input type="text" id="sponsorName" name="sponsorName" placeholder="My Awesome Company"/></td>
</tr>
<tr>
<td><label for="sponsorURL">Sponsor URL (optional)</label></td><td><input type="text" id="sponsorURL" name="sponsorURL" placeholder="https://awesomecompany.com"/></td>
</tr>
</table>
<script type="text/javascript">enableDisableSponsorForm()</script>
<input type="checkbox" id="check1" name="check1"/><label for="check1">My server has a bandwidth of at least 1gbps , no throttling or traffic restrictions and can handle a relatively high amount of traffic</label><br/>
<input type="checkbox" id="check2" name="check2"/><label for="check2">I understand that my server will be publicly visible to anyone visiting LibreSpeed.org, and potentially exposed to abuse and/or attacks</label><br/><br/>
<label for="captcha_code" >Prove that you're human</label>&nbsp;<input type="text" id="captcha_code" name="captcha_code" autocomplete="off"/><br/>
<img id="captcha" src="securimage/securimage_show.php" alt="CAPTCHA Image"/><br/>
<?php if(isset($_SESSION["logged"])) echo "Admins can ignore the captcha<br/>"; ?>
<input type="submit" value="Submit for review"/>
</form>
</body>
</html>

