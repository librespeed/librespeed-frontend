<?php
session_start();
include_once '../_config.php';
?>
<!DOCTYPE html>
<html>
<head>
<title>Server submissions - LibreSpeed</title>
<link rel="stylesheet" type="text/css" href="../style.css" />
<style type="text/css">
table.borders{
    margin:1em 0;
    width:100%;
}
table.borders,table.borders tr,table.borders th,table.borders td{
    border:1px solid #CCCCCC;
}
</style>
</head>
<body>
<h2>Server submissions</h2>
<?php
if(!isset($_SESSION["logged"])){
    if(isset($_POST["password"])){
        if($_POST["password"]===$admin_password){
            $_SESSION["logged"]=true;
?>
            Logged in<br/><br/>
            <a href="admin.php">Continue</a>
            <script type="text/javascript">location.href="admin.php"</script>
<?php
        }else{
?>
            Wrong password<br/><br/>
            <a href="admin.php">Continue</a>
<?php
        }
    }else{
?>
        <form method="POST" action="admin.php">
            <table>
                <tr><td><label for="password">Password</label></td><td><input type="password" id="password" name="password" /></td>
                </tr>
            </table>
            <input type="submit" value="Log in" />
        </form>
<?php  
    }
}else{
    try{
        $conn = new mysqli($MySql_hostname, $MySql_username, $MySql_password, $MySql_databasename);
        if(!$conn) throw new Exception("Failed to connect");
        if(isset($_GET["op"])){
            if($_GET["op"]==="approve"){
                $stmt = $conn->prepare("update submissions set status=10 where id=?");
                if(!$stmt) throw new Exception("Failed to create statement");
                if(!$stmt->bind_param("i",$_GET["id"])) throw new Exception("Failed to bind params");
                if(!$stmt->execute()) throw new Exception("Failed to execute update");
                $stmt->close();
                $conn->close();
                ?>
                    Done<br/><br/>
                    <a href="admin.php">Continue</a>
                    <script type="text/javascript">location.href="admin.php"</script>
                <?php
            }
            if($_GET["op"]==="reject"){
                $stmt = $conn->prepare("update submissions set status=20 where id=?");
                if(!$stmt) throw new Exception("Failed to create statement");
                if(!$stmt->bind_param("i",$_GET["id"])) throw new Exception("Failed to bind params");
                if(!$stmt->execute()) throw new Exception("Failed to execute update");
                $stmt->close();
                $conn->close();
                ?>
                    Done<br/><br/>
                    <a href="admin.php">Continue</a>
                    <script type="text/javascript">location.href="admin.php"</script>
                <?php
            }
            if($_GET["op"]==="delete"){
                $stmt = $conn->prepare("delete from submissions where id=?");
                if(!$stmt) throw new Exception("Failed to create statement");
                if(!$stmt->bind_param("i",$_GET["id"])) throw new Exception("Failed to bind params");
                if(!$stmt->execute()) throw new Exception("Failed to execute delete");
                $stmt->close();
                $conn->close();
                ?>
                    Done<br/><br/>
                    <a href="admin.php">Continue</a>
                    <script type="text/javascript">location.href="admin.php"</script>
                <?php
            }
            if($_GET["op"]==="show"){
                $stmt = $conn->prepare("select id,name,server,dlURL,ulURL,pingURL,getIpURL,email,fullName,type,status,sponsorName,sponsorURL from submissions where id=?");
                if(!$stmt) throw new Exception("Failed to create statement");
                if(!$stmt->bind_param("i",$_GET["id"])) throw new Exception("Failed to bind params");
                if(!$stmt->execute()) throw new Exception("Failed to execute query");
                $stmt->store_result();
                $stmt->bind_result($id,$name,$server,$dlURL,$ulURL,$pingURL,$getIpURL,$email,$fullName,$type,$status,$sponsorName,$sponsorURL);
                if($stmt->fetch()){
?>
                    <h3>Edit submission</h3>
                    <a href="admin.php">Back</a><br/><br/>
                    <form action="admin.php?op=edit" method="POST">
                    <input type="hidden" name="id" value="<?=$id?>" />
                    <table>
                    <tr>
                    <td><label for="name">User friendly name</label></td><td><input type="text" id="name" name="name" placeholder="Location, Country (Provider)" value="<?=htmlentities($name,ENT_QUOTES)?>"/></td>
                    </tr>
                    <tr>
                    <td><label for="server">URL to the server</label></td><td><input type="text" id="server" name="server" placeholder="https://yourDomain.com/" value="<?=htmlentities($server,ENT_QUOTES)?>"/></td>
                    </tr>
                    <tr>
                    <td><label for="dlURL">Path to download test</label></td><td><input type="text" id="dlURL" name="dlURL" placeholder="garbage.php" value="<?=htmlentities($dlURL,ENT_QUOTES)?>"/></td>
                    </tr>
                    <tr>
                    <td><label for="ulURL">Path to upload test</label></td><td><input type="text" id="ulURL" name="ulURL" placeholder="empty.php" value="<?=htmlentities($ulURL,ENT_QUOTES)?>"/></td>
                    </tr>
                    <tr>
                    <td><label for="pingURL">Path to ping/jitter test</label></td><td><input type="text" id="pingURL" name="pingURL" placeholder="empty.php" value="<?=htmlentities($pingURL,ENT_QUOTES)?>"/></td>
                    </tr>
                    <tr>
                    <td><label for="getIpURL">Path to getIP</label></td><td><input type="text" id="getIpURL" name="getIpURL" placeholder="getIP.php" value="<?=htmlentities($getIpURL,ENT_QUOTES)?>"/></td>
                    </tr>
                    <tr>
                    <td><label for="type">Type of backend</label></td><td><select name="type" id="type" value="<?=htmlentities($type,ENT_QUOTES)?>"><option value="php">PHP</option><option value="php_docker">PHP (Docker)</option><option value="go">Go</option><option value="node">Node</option><option value="custom">Custom</option></select></td>
                    </tr>
                    <tr>
                    <td><label for="email">Email address</label></td><td><input type="text" id="email" name="email"  placeholder="john.doe@email.com" value="<?=htmlentities($email,ENT_QUOTES)?>"/></td>
                    </tr>
                    <tr>
                    <td><label for="fullName">User's full name</label></td><td><input type="text" id="fullName" name="fullName"  placeholder="John Doe" value="<?=htmlentities($fullName,ENT_QUOTES)?>"/></td>
                    </tr>
                    <tr>
                    <td><label for="sponsorName">Sponsor name</label></td><td><input type="text" id="sponsorName" name="sponsorName"  placeholder="My Awesome Company" value="<?=htmlentities($sponsorName,ENT_QUOTES)?>"/></td>
                    </tr>
                    <tr>
                    <td><label for="sponsorURL">Sponsor URL</label></td><td><input type="text" id="sponsorURL" name="sponsorURL"  placeholder="https://awesomecompany.com" value="<?=htmlentities($sponsorURL,ENT_QUOTES)?>"/></td>
                    </tr>
                    <tr>
                    <td><label for="status">Status</label></td><td><select name="status" id="status" value="<?=$status?>"><option value="0" <?=$status==0?"selected='true'":""?>>Submitted</option><option value="10" <?=$status==10?"selected='true'":""?>>Enabled</option><option value="20" <?=$status==20?"selected='true'":""?>>Disabled</option></select></td>
                    </tr>
                    </table>
                    <input type="submit" value="Save"/>
                    </form><br/><br/>
                    <a href="admin.php?op=delete&id=<?=$id?>">Delete submission</a>
<?php
                }
            }
            if($_GET["op"]==="edit"){
                $stmt = $conn->prepare("update submissions set name=?,server=?,dlURL=?,ulURL=?,pingURL=?,getIpURL=?,email=?,fullName=?,type=?,status=?,sponsorName=?,sponsorURL=? where id=?");
                if(!$stmt) throw new Exception("Failed to create statement");
                if(!$stmt->bind_param("sssssssssissi",$_POST["name"],$_POST["server"],$_POST["dlURL"],$_POST["ulURL"],$_POST["pingURL"],$_POST["getIpURL"],$_POST["email"],$_POST["fullName"],$_POST["type"],$_POST["status"],$_POST["sponsorName"],$_POST["sponsorURL"],$_POST["id"])) throw new Exception("Failed to bind params");
                if(!$stmt->execute()) throw new Exception("Failed to execute update");
                $stmt->close();
                if(empty($_POST["sponsorName"])){
                    $stmt = $conn->prepare("update submissions set sponsorName=NULL where id=?");
                    if(!$stmt) throw new Exception("Failed to create statement");
                    if(!$stmt->bind_param("i",$_POST["id"])) throw new Exception("Failed to bind params");
                    if(!$stmt->execute()) throw new Exception("Failed to execute update");
                    $stmt->close();
                }
                if(empty($_POST["sponsorURL"])){
                    $stmt = $conn->prepare("update submissions set sponsorURL=NULL where id=?");
                    if(!$stmt) throw new Exception("Failed to create statement");
                    if(!$stmt->bind_param("i",$_POST["id"])) throw new Exception("Failed to bind params");
                    if(!$stmt->execute()) throw new Exception("Failed to execute update");
                    $stmt->close();
                }
                $conn->close();
                ?>
                    Done<br/><br/>
                    <a href="admin.php">Continue</a>
                    <script type="text/javascript">location.href="admin.php"</script>
                <?php
            }
        }else{
?>
            Display: <a href="admin.php?status=0">Submitted</a> | <a href="admin.php?status=10">Enabled</a> | <a href="admin.php?status=20">Disabled</a><br/><br/>
            <a href="index.php">New submission</a><br/>
            <h3>List</h3>
<?php
            $qstatus=0;
            if(isset($_GET["status"])) $qstatus=$_GET["status"];
            $stmt = $conn->prepare("select id,name,server,dlURL,ulURL,pingURL,getIpURL,email,fullName,type,status,sponsorName,sponsorURL from submissions where status=?");
            if(!$stmt) throw new Exception("Failed to create statement");
            if(!$stmt->bind_param("i",$qstatus)) throw new Exception("Failed to bind params");
            if(!$stmt->execute()) throw new Exception("Failed to execute query");
            $stmt->store_result();
            $stmt->bind_result($id,$name,$server,$dlURL,$ulURL,$pingURL,$getIpURL,$email,$fullName,$type,$status,$sponsorName,$sponsorURL);
            while(true){
                if(!$stmt->fetch()) break;
?>
                <table class="borders">
                <tr><th>Status</th><td><?php
                    switch($status){
                        case 0: echo "Submitted"; break;
                        case 10: echo "Enabled"; break;
                        case 20: echo "Disabled"; break;
                        default: echo "Invalid"; break;
                    }
                ?></td></tr>
                <tr><th>User friendly name</th><td><?=nl2br(htmlspecialchars($name))?></td></tr>
                <tr><th>URL to the server</th><td><?=nl2br(htmlspecialchars($server))?></td></tr>
                <tr><th>Path to download test</th><td><?=nl2br(htmlspecialchars($dlURL))?></td></tr>
                <tr><th>Path to upload test</th><td><?=nl2br(htmlspecialchars($ulURL))?></td></tr>
                <tr><th>Path to ping/jitter test</th><td><?=nl2br(htmlspecialchars($pingURL))?></td></tr>
                <tr><th>Path to getIP</th><td><?=nl2br(htmlspecialchars($getIpURL))?></td></tr>
                <tr><th>Type of backend</th><td><?=nl2br(htmlspecialchars($type))?></td></tr>
                <tr><th>Email address</th><td><?=nl2br(htmlspecialchars($email))?></td></tr>
                <tr><th>User's full name</th><td><?=nl2br(htmlspecialchars($fullName))?></td></tr>
                <?php
                if(!is_null($sponsorName)){
                ?>
                        <tr><th>Sponsor Name</th><td><?=nl2br(htmlspecialchars($sponsorName))?></td></tr>
                <?php
                }
                if(!is_null($sponsorURL)){
                ?>
                        <tr><th>Sponsor URL</th><td><?=nl2br(htmlspecialchars($sponsorURL))?></td></tr>
                <?php
                }
                ?>
                <tr><th>Actions</th></td><td><a href="admin.php?op=show&id=<?=$id?>">Edit</a> | <?php if($_GET["status"]!=10) { ?><a href="admin.php?op=approve&id=<?=$id?>">Enable</a>  | <?php } ?><?php if($_GET["status"]!=20) { ?><a href="admin.php?op=reject&id=<?=$id?>">Disable</a> | <?php } ?><a href="admin.php?op=delete&id=<?=$id?>">Delete</a></td></tr>
                </table>
<?php
            }
            $stmt->close();
            $conn->close();
        }
    }catch(Exception $e){
        echo "An error occurred: $e";
    }
}
?>
</body>
</html>
