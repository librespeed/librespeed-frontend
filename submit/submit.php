<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
<title>Submit a server - LibreSpeed</title>
<link rel="stylesheet" type="text/css" href="../style.css" />
</head>
<body>
<h2>Submit a server</h2>
<?php
include_once 'securimage/securimage.php';
$securimage=new Securimage();
if(!isset($_SESSION["logged"])&&(!isset($_POST["captcha_code"])||$securimage->check($_POST["captcha_code"])==false)){
?>
Captcha error. <a href="javascript:window.history.back()">Try again</a>
<?php
}else{
include_once '../_config.php';
    try{
        $conn = new mysqli($MySql_hostname, $MySql_username, $MySql_password, $MySql_databasename);
        if(!$conn) throw new Exception("Failed to connect");
        $stmt=NULL;
        if($_POST["isSponsored"]){
            $stmt = $conn->prepare("INSERT INTO submissions (name,server,dlURL,ulURL,pingURL,getIpURL,email,fullName,type,sponsorName,sponsorURL) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
            if(!$stmt) throw new Exception("Failed to create statement");
            if(!$stmt->bind_param("sssssssssss",$_POST["name"],$_POST["server"],$_POST["dlURL"],$_POST["ulURL"],$_POST["pingURL"],$_POST["getIpURL"],$_POST["email"],$_POST["fullName"],$_POST["type"],$_POST["sponsorName"],$_POST["sponsorURL"])) throw new Exception("Failed to bind params");
            if(!$stmt->execute()) throw new Exception("Failed to execute insert");
            $stmt->close();
        }else{
            $stmt = $conn->prepare("INSERT INTO submissions (name,server,dlURL,ulURL,pingURL,getIpURL,email,fullName,type) VALUES (?,?,?,?,?,?,?,?,?)");
            if(!$stmt) throw new Exception("Failed to create statement");
            if(!$stmt->bind_param("sssssssss",$_POST["name"],$_POST["server"],$_POST["dlURL"],$_POST["ulURL"],$_POST["pingURL"],$_POST["getIpURL"],$_POST["email"],$_POST["fullName"],$_POST["type"])) throw new Exception("Failed to bind params");
            if(!$stmt->execute()) throw new Exception("Failed to execute insert");
            $stmt->close();
        }
        $conn->close();
?>
Thanks for submitting your server. We'll check your submission and contact you back via the provided email address.<br/><br/>
<a href="/">Continue</a>
<?php
	}catch(Exception $e){
?>
Something happened and your submission couldn't be registered. This is likely a problem on our end. Please <a href="mailto:info@librespeed.org">send us an email</a> and we'll help you out.<br/><br/>
<a href="/">Continue</a>
<?php
	}
}
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
include_once 'PHPMailer/src/PHPMailer.php';
include_once 'PHPMailer/src/SMTP.php';
$mail=new PHPMailer(true);
try{
    $mail->isSMTP();
    $mail->Host=$SMTP_hostname;
    $mail->SMTPAuth=true;
    $mail->Username=$SMTP_username;
    $mail->Password=$SMTP_password;
    $mail->SMTPSecure=PHPMailer::ENCRYPTION_STARTTLS;
    $mail->Port=$SMTP_port;
    $mail->setFrom($Email_from);
    foreach($Email_to as $a){
        $mail->addAddress($a);
    }
    $mail->isHTML(true);
    $mail->Subject="New backend submitted";
    $body="A user has submitted a backend for review<br/><br/>
    <table>
        <tr><th>User friendly name: </th><td>".nl2br(htmlspecialchars($_POST['name']))."</td></tr>
        <tr><th>URL to the server: </th><td>".nl2br(htmlspecialchars($_POST['server']))."</td></tr>
        <tr><th>Path to download test: </th><td>".nl2br(htmlspecialchars($_POST['dlURL']))."</td></tr>
        <tr><th>Path to upload test: </th><td>".nl2br(htmlspecialchars($_POST['ulURL']))."</td></tr>
        <tr><th>Path to ping/jitter test: </th><td>".nl2br(htmlspecialchars($_POST['pingURL']))."</td></tr>
        <tr><th>Path to getIP: </th><td>".nl2br(htmlspecialchars($_POST['getIpURL']))."</td></tr>
        <tr><th>Type of backend: </th><td>".nl2br(htmlspecialchars($_POST['type']))."</td></tr>
    </table>
    <br/>";
    if($_POST["isSponsored"]){
        $body.="Sponsored by ";
        $body.=nl2br(htmlspecialchars($_POST['sponsorName']));
        if(!empty($_POST["sponsorURL"])) $body.=" (".nl2br(htmlspecialchars($_POST['sponsorURL'])).")";
        $body.="<br/><br/>";
    }
    $body.="Please check the backend and contact the user at <a href='mailto:".htmlentities($_POST['email'],ENT_QUOTES)."'>".htmlspecialchars($_POST['email'])."</a>, the user's name is ".htmlspecialchars($_POST['fullName']);
    $mail->Body=$body;
    $mail->send();
}catch(Exception $e){
}
?>
</body>
</html>
